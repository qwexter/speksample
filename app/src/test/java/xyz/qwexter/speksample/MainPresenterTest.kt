package xyz.qwexter.speksample

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import xyz.qwexter.speksample.utils.SpekRxSchedulers

class MainPresenterTest : Spek({

    val rxLifecycleListener = SpekRxSchedulers()

    registerListener(rxLifecycleListener)

    describe("Main Presenter test") {

        val view by memoized {
            Mockito.mock<MainView>(MainView::class.java)
        }

        val addOperator by memoized {
            AddOperator()
        }

        val presenter by memoized {
            MainPresenter(view, addOperator)
        }

        val lifecycleOwner by memoized {
            Mockito.mock(LifecycleOwner::class.java)
        }

        lateinit var lifecycle: LifecycleRegistry

        beforeEach {
            lifecycle = LifecycleRegistry(lifecycleOwner)
            lifecycle.addObserver(presenter)
        }

        afterEach {
            lifecycle.removeObserver(presenter)
        }

        describe("Проверяем негативные сценарии,когда не происходит вызовов к View") {
            it("Первый аргумент - пустая строка") {
                presenter.calc(inputFirst = "", inputSecond = "0")
                Mockito.verifyZeroInteractions(view)
            }
            it("Второй аргумент - пустая строка") {
                presenter.calc(inputFirst = "0", inputSecond = "")
                Mockito.verifyZeroInteractions(view)
            }
            it("Оба агрумента - пустые строки") {
                presenter.calc(inputFirst = "", inputSecond = "")
                Mockito.verifyZeroInteractions(view)
            }
            it("Первый аргумент содержит некорректные символы") {
                presenter.calc(inputFirst = "0!", inputSecond = "0")
                Mockito.verifyZeroInteractions(view)
            }
            it("Второй аргумент содержит некорректные символы") {
                presenter.calc(inputFirst = "0", inputSecond = "0!")
                Mockito.verifyZeroInteractions(view)
            }
            it("Оба аргумента содержат некорректные символы") {
                presenter.calc(inputFirst = "0!", inputSecond = "0!")
                Mockito.verifyZeroInteractions(view)
            }
        }

        describe("Проверяем позитивные сценарии сложения аргументов") {
            listOf(
                Triple(0, 0, 0),
                Triple(1, 0, 1),
                Triple(0, 1, 1),
                Triple(100, 1, 101),
                Triple(100, -1, 99)
            ).forEach { (first, second, result) ->
                it("Вызывается showResult c значением равным: $first + $second = $result") {
                    presenter.calc(first.toString(), second.toString())
                    rxLifecycleListener.triggerAll()
                    Mockito.verify(view).showResult(eq(result))
                    Mockito.verifyNoMoreInteractions(view)
                }
            }
        }

    }
})

