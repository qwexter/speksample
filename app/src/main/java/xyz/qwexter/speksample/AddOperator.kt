package xyz.qwexter.speksample

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

import io.reactivex.schedulers.Schedulers


open class AddOperator : CalcOperator {

    override fun calc(left: Int, right: Int): Single<Int> = Single.fromCallable {
        return@fromCallable left + right
    }.observeOn(Schedulers.computation())
        .subscribeOn(AndroidSchedulers.mainThread())

}