package xyz.qwexter.speksample

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

class MainPresenter(
    view: MainView,
    private val operator: CalcOperator
) : LifecycleObserver {

    private val subscriptions = CompositeDisposable()
    private val viewRef = WeakReference<MainView>(view)

    fun calc(inputFirst: String, inputSecond: String) {
        val valueFirst = inputFirst.toIntOrNull()
        val valueSecond = inputSecond.toIntOrNull()
        if (valueFirst == null || valueSecond == null) {
            return
        }
        subscriptions.add(
            operator.calc(valueFirst, valueSecond).subscribe { result ->
                viewRef.get()?.showResult(result)
            }
        )
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onOnDestroy(source: LifecycleOwner) {
        subscriptions.clear()
    }

}