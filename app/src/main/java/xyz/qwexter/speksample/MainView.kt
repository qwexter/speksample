package xyz.qwexter.speksample

interface MainView {
    fun showResult(result: Int)
}